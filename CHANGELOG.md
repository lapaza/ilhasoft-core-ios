# Ilhasoft Core CHANGELOG

## Version 0.0.8 (2016-06-04)

* Create ISButtonImagePicker;
* Add progressHUD dependency.

## Version 0.0.7 (2016-05-29)

* Update ISImageView to accept initWithFrame;
* Create UIViewController extension.

## Version 0.0.5 (2016-05-15)

* Create ISImageView component;

## Version 0.0.3 (2016-05-15)

* Update ISTemsViewController.xib to iPadScreen;
* Make ISModalViewController initWithNibName method public;
* Create ISLocalPersistenceManager;
* Include localized strings in library resources;
* Added ISViewControllerUtil containing a property for screen size and a function to switch the rootViewController;
* Create class ISCoreConstants;
* Add completion block to ISMOdalViewController.show()
* Remove Parse dependency.

## Versio 0.0.2 (2016-04-13)

* Make all classes public;
* Configure podspec file.

## Version 0.0.1 (2016-04-10)

* Create UIColor, NSDate, UIImage, UILabel and NSString extensions;
* Create ISTableView and ISTableViewCell.
