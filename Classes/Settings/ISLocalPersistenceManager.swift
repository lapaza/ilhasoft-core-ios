//
//  ISPersistenceManager.swift
//  IlhasoftCore
//
//  Created by Dielson Sales de Carvalho on 02/05/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

open class ISLocalPersistenceManager: NSObject {

    /*
    * Stores an object as a setting in the specified key
    */
    open static func saveElement<T:AnyObject>(_ element:T, forKey key:String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(element, forKey: key)
        defaults.synchronize()
    }

    /*
    * Retrieves an object stored as a setting in the specified key
    */
    open static func getElement<T:AnyObject>(_ key: String) -> T? {
        let defaults: UserDefaults = UserDefaults.standard
        let element = defaults.object(forKey: key) as? T
        return element
    }

    /*
    * Stores a boolean property in the NSUserDefaults settings.
    */
    open static func saveBooleanElement(_ booleanElement:Bool, forKey key:String) {
        let element: NSNumber = NSNumber(booleanLiteral: booleanElement)
        saveElement(element, forKey: key)
    }

    /*
    * Retrieves the saved boolean property, or false if no object is found.
    */
    open static func getBooleanElement(_ key: String) -> Bool {
        let element: NSNumber? = getElement(key)
        if let element = element {
            return element.boolValue
        } else {
            return false
        }
    }

    /*
     * Stores a string property in the NSUserDefaults settings
     */
    open static func saveString(_ stringElement: String, forKey key: String) {
        let defaults: UserDefaults = UserDefaults.standard
        defaults.set(stringElement, forKey: key)
        defaults.synchronize()
    }

    /*
     * Retrieves a string property from the NSUserDefaults settings
     */
    open static func getString(_ key: String) -> String? {
        let defaults: UserDefaults = UserDefaults.standard
        return defaults.object(forKey: key) as? String
    }
}
