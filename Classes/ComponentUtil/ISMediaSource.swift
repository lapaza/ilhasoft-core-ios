//
//  F2NMediaSource.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public enum ISMediaSource {
    case Gallery
    case Camera
    case Video
    case Audio
    case File
    case Youtube
    
    public static var allMediaSources: [ISMediaSource] {
        get {
            return [
                .Gallery,
                .Camera,
                .Video,
                .Audio,
                .File,
                .Youtube
            ]
        }
    }
    
}

