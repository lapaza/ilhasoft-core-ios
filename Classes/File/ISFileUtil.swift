//
//  ISFileUtil.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 02/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

open class ISFileUtil: NSObject {

    public static let outPutURLDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
    
    open class func removeFile(_ fileURL: URL) {
        let filePath = fileURL.path
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: filePath) {
            do {
                try fileManager.removeItem(atPath: filePath)
            }catch let error as NSError {
                print("Can't remove file \(error.localizedDescription)")
            }
            
        }else{
            print("file doesn't exist")
        }
    }
    
    open class func writeAudioFile(_ data:Data) -> String{
        
        let path = ISFileUtil.outPutURLDirectory.appendingPathComponent("audio.3gp")
        
        ISFileUtil.removeFile(URL(string: path)!)
        
        if ((try? data.write(to: URL(fileURLWithPath: path), options: [.atomic])) != nil) == true {
            print("file available")
        }
        
        return path
    }
    
}
