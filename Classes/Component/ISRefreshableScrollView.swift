//
//  ISRefreshableScrollView.swift
//  IlhasoftCore
//
//  Created by Dielson Sales on 13/06/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

public protocol ISRefreshableScrollViewDelegate {
    func onRefresh()
}

open class ISRefreshableScrollView: TPKeyboardAvoidingScrollView {

    open var _refreshControl = UIRefreshControl()
    var refreshableScrollViewDelegate: ISRefreshableScrollViewDelegate?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        _refreshControl.addTarget(self, action: #selector(ISRefreshableScrollView.onRefresh), for: UIControlEvents.valueChanged)
        addSubview(_refreshControl)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        _refreshControl.addTarget(self, action: #selector(ISRefreshableScrollView.onRefresh), for: UIControlEvents.valueChanged)
        addSubview(_refreshControl)
    }

    func onRefresh() {
        _refreshControl.attributedTitle = NSAttributedString(string: ISCoreConstants.localizedStringForKey("is_refresh_data"))
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            self.refreshableScrollViewDelegate?.onRefresh()
            DispatchQueue.main.async {
                self._refreshControl.endRefreshing()
            }
        }
    }
}
