//
//  ISTextFieldPicker.swift
//  Pods
//
//  Created by Daniel Amaral on 25/06/16.
//
//

import UIKit
import Alamofire
import AlamofireObjectMapper
/*
open class ISTextFieldAlamofirePicker: SRKComboBox {
    
    @IBInspectable open var sourceURL: String!
    
    @IBInspectable open var dataKey: String!
    
    @IBInspectable open var attributeName: String!
    
    open var dataArray = [String]()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open func loadData() {
        if sourceURL.characters.count == 0 && attributeName.characters.count == 0 {
            print("You didn't setup sourceURL and fieldName")
            return
        }

        Alamofire.request(sourceURL).responseJSON { response in // method defaults to `.get`
            if let response = response.result.value {

                var json = response

                if self.dataKey.characters.count > 0 {
                    json = (json as! NSDictionary)[self.dataKey] as! [NSDictionary]
                }else {
                    json = json as! [NSDictionary]
                }

                for dictionary in json as! [NSDictionary] {
                    self.dataArray.append(dictionary[self.attributeName] as! String)
                }
                
            }else {
                
            }
        }
        
    }
   
}
*/
