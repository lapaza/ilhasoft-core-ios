//
//  ISImageView.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 14/05/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import MobileCoreServices

@objc public protocol ISImageViewPickerDelegate {
    func mediaDidLoad(imageView:ISImageViewPicker,media:ISMedia)
    func mediaDidRemove(imageView:ISImageViewPicker,media:ISMedia)
}

open class ISImageViewPicker: UIImageView, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public var parentViewController:UIViewController!
    public var delegate:ISImageViewPickerDelegate?
    var defaultPlaceHolderImage:UIImage?
    public var videoMaximumDuration:Int? = 15
    public var media:ISMedia?
    public var mediaSources:[ISMediaSource]?
    
    @IBInspectable public var placeHolderImageName: String = "" {
        didSet {
            self.defaultPlaceHolderImage = UIImage(named: placeHolderImageName)
            self.tag = 500
            self.image = defaultPlaceHolderImage
            self.contentMode = .center
        }
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        addGesture()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        addGesture()
    }
    
    //MARK: Class Methods
    
    fileprivate func addGesture() {                
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(changePicture))
        tapGesture.numberOfTapsRequired = 1
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)
    }
    
    func changePicture() {
        let changePicturePicker = ISChangePicturePicker(parentViewController: parentViewController, imagePickerProtocol: self, view: self)
        changePicturePicker.mediaSources = self.mediaSources
        changePicturePicker.videoMaximumDuration = videoMaximumDuration
        changePicturePicker.changePicture()
    }
    
    //MARK: ImagePickerControllerDelegate
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType == kUTTypeMovie {
            
            let mediaURL = (info[UIImagePickerControllerMediaURL] as! NSURL)
            let path = mediaURL.path
            
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path!) {}
            
            let media = ISVideoMedia()
            media.path = path
            media.thumbnailImage = ISVideoUtil.generateThumbnail(mediaURL as URL)
            self.media = media
            
            self.image = media.thumbnailImage
            self.tag = 0
            
            delegate?.mediaDidLoad(imageView: self, media: media)
            
        }else {
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                self.image = pickedImage
                self.tag = 0
                
                let media = ISImageMedia()
                media.image = pickedImage
                self.media = media
                
                delegate?.mediaDidLoad(imageView: self, media: media)
            }
        }
        
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}
