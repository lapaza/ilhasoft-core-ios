//
//  ISButtonImagePicker.swift
//  Pods
//
//  Created by Daniel Amaral on 06/06/16.
//
//

import UIKit

open class ISButtonImagePicker: UIButton {

    public var parentViewController:UIViewController!
    public var videoMaximumDuration:Int? = 15
    public var media:ISMedia?
    public var mediaSources:[ISMediaSource]? = [.Gallery, .Camera]{
        didSet {
            changePicture()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.addAction()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addAction()
    }
    

    //MARK: Class Methods
    
    fileprivate func addAction() {
        self.addTarget(self, action: #selector(changePicture), for: UIControlEvents.touchUpInside)
    }
    
    func changePicture() {
        
        if let parentViewController = parentViewController as? UIImagePickerControllerDelegate & UINavigationControllerDelegate, let viewController = parentViewController as? UIViewController {
            let changePicturePicker = ISChangePicturePicker(parentViewController: viewController, imagePickerProtocol: parentViewController, view: self)
            changePicturePicker.mediaSources = self.mediaSources
            changePicturePicker.changePicture()
        }
    }
    
}
