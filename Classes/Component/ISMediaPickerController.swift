//
//  CCMediaPickerController.swift
//  Pods
//
//  Created by Daniel Amaral on 02/12/16.
//
//

import UIKit
import MobileCoreServices
import Proposer
import SDWebImage

@objc public protocol ISMediaPickerControllerDelegate {
    func mediaDidLoad(media:ISMedia)
}

public class ISMediaPickerController: UIViewController {

    public var delegate:ISMediaPickerControllerDelegate?
    public var videoMaximumDuration:Int? = 15
    public var media:ISMedia?
    public var mediaSources:[ISMediaSource]?
    
    public var showInViewController:UIViewController!
    let imagePicker = UIImagePickerController()

    public init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.view.backgroundColor = UIColor.clear
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    public func show(in viewController:UIViewController) {
        self.showInViewController = viewController
        createAlertController()
    }
    
    public func createAlertController() {
        
        proposeToAccess(PrivateResource.photos, agreed: {
            
            if self.mediaSources == nil {
                self.mediaSources = ISMediaSource.allMediaSources
            }
            
            let alert = UIAlertController(title: nil, message: ISCoreConstants.localizedStringForKey("choice_option"), preferredStyle: UIAlertControllerStyle.actionSheet)
            
            if let _ = self.mediaSources!.index(of: .Gallery) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_gallery"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    //var delegate = self
                    self.imagePicker.delegate = self
                    self.imagePicker.allowsEditing = false;
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.mediaTypes = [kUTTypeMovie as String,"public.image"]
                    self.showInViewController.present(self.imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Camera) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_take_picture"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
                    self.imagePicker.showsCameraControls = true
                    self.imagePicker.allowsEditing = true
                    self.showInViewController.present(self.imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Video) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_video"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.mediaTypes = [kUTTypeMovie as String]
                    self.imagePicker.videoQuality = .type640x480
                    self.imagePicker.videoMaximumDuration = Double(self.videoMaximumDuration!)
                    self.imagePicker.allowsEditing = true
                    self.showInViewController.present(self.imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Audio) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_audio"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    let audioRecorder = ISAudioRecorderViewController(audioURL: nil)
                    audioRecorder.delegate = self
                    self.showInViewController.present(audioRecorder, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Youtube) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_youtube"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    let alertControllerTextField = UIAlertController(title: nil, message: ISCoreConstants.localizedStringForKey("message_youtube_link"), preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertControllerTextField.addTextField(configurationHandler: nil)
                    alertControllerTextField.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_cancel"), style: .cancel, handler: nil))
                    alertControllerTextField.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) -> Void in
                        
                        let urlVideo = alertControllerTextField.textFields![0].text!
                        
                        if urlVideo.isEmpty {
                            ISAlertMessages.displayErrorMessage(ISCoreConstants.localizedStringForKey("error_empty_link"), fromController: self.showInViewController)
                            return
                        }
                        
                        guard let videoID = ISYoutubeUtil.getYoutubeVideoID(urlVideo) else {
                            ISAlertMessages.displayErrorMessage(ISCoreConstants.localizedStringForKey("error_empty_link"), fromController: self.showInViewController)
                            return
                        }
                        
                        let media = ISYoutubeMedia()
                        
                        media.id = videoID
                        media.url = "http://img.youtube.com/vi/%@/mqdefault.jpg".replacingOccurrences(of: "%@", with: media.id!)
                        
                        SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string:media.url!), options: .highPriority, progress: {(receivedSize: Int, expectedSize: Int) -> Void in
                        }, completed: { (image: UIImage?, data: Data?, error: Error?, finish: Bool) -> Void in
                            if let image = image {
                                media.thumbnailImage = image
                                self.delegate?.mediaDidLoad(media: media)
                            }else{
                                print("error on image download")
                            }
                        })
                        
                    }))
                    
                    self.showInViewController.present(alertControllerTextField, animated: true, completion: nil)
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_cancel"), style: UIAlertActionStyle.cancel, handler: { (alertAction) in }))
            
            
            if ISCoreConstants.isIpad {
                alert.modalPresentationStyle = UIModalPresentationStyle.popover
                alert.popoverPresentationController!.sourceView = self.view
                alert.popoverPresentationController!.sourceRect = self.view.bounds
            }
            
            self.showInViewController.present(alert, animated: true, completion: {})
            
        }, rejected: {
            self.alertNoPermissionToAccess(PrivateResource.photos)
        })
    }
    
}

extension ISMediaPickerController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType == kUTTypeMovie {
            
            let mediaURL = (info[UIImagePickerControllerMediaURL] as! NSURL)
            let path = mediaURL.path
            
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path!) {}
            
            let media = ISVideoMedia()
            media.path = path
            media.thumbnailImage = ISVideoUtil.generateThumbnail(mediaURL as URL)
            self.media = media
            
            delegate?.mediaDidLoad(media: media)
            
        }else {
            
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                let media = ISImageMedia()
                media.image = pickedImage
                self.media = media
                
                delegate?.mediaDidLoad(media: media)
            }
        }
                
        picker.dismiss(animated: true, completion: nil)
        
    }
    
}

extension ISMediaPickerController : ISAudioRecorderViewControllerDelegate {
    public func newAudioRecorded(_ ISAudioRecorderViewController: ISAudioRecorderViewController, media: ISMedia) {
        delegate?.mediaDidLoad(media: media)
    }
}
