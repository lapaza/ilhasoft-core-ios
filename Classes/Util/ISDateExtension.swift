//
//  ISDateExtension.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 26/08/15.
//  Copyright (c) 2015 ilhasoft. All rights reserved.
//

import UIKit

public extension Date {
    public func yearsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: date, to: self, options: []).year!
    }
    public func monthsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.month, from: date, to: self, options: []).month!
    }
    public func weeksFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    public func daysFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.day, from: date, to: self, options: []).day!
    }
    public func hoursFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.hour, from: date, to: self, options: []).hour!
    }
    public func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.minute, from: date, to: self, options: []).minute!
    }
    public func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(NSCalendar.Unit.second, from: date, to: self, options: []).second!
    }
    public func offsetFrom(_ date:Date) -> (offset:Int?,calendarUnit:NSCalendar.Unit?) {
        if yearsFrom(date)   > 0 { return (yearsFrom(date), .year)}
        if monthsFrom(date)  > 0 { return (monthsFrom(date), .month)}
        if weeksFrom(date)   > 0 { return (weeksFrom(date), .weekOfYear)}
        if daysFrom(date)    > 0 { return (daysFrom(date), .day)}
        if hoursFrom(date)   > 0 { return (hoursFrom(date), .hour)}
        if minutesFrom(date) > 0 { return (minutesFrom(date), .minute)}
        if secondsFrom(date) > 0 { return (secondsFrom(date), .second)}
        return (nil,nil)
    }
    public static func localizedOffsetFrom(_ date:Date) -> String {
        let offsetValue: Int?, offsetUnit: NSCalendar.Unit?
        (offsetValue, offsetUnit) = Date().offsetFrom(date)
        return localizedStringWithOffset(offsetValue, calendarUnit: offsetUnit)
    }
    static func localizedStringWithOffset(_ offset:Int?, calendarUnit:NSCalendar.Unit?) -> String {
        if let offset = offset, let calendarUnit = calendarUnit {
            func formatString(_ key: String, withOffset offset: Int?) -> String {
                if let offset = offset {
                    let formatString = ISCoreConstants.localizedStringForKey(key)
                    return String(format:formatString, offset)
                }
                return ISCoreConstants.localizedStringForKey(key)
            }
            switch(offset, calendarUnit) {
            case (offset, NSCalendar.Unit.year) where offset == 1:
                return  formatString("year_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.year) where offset > 1:
                return  formatString("years_plural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.month) where offset == 1:
                return  formatString("month_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.month) where offset > 1:
                return  formatString("months_plural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.weekOfYear) where offset == 1:
                return  formatString("week_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.weekOfYear) where offset > 1:
                return  formatString("weeks_plural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.day) where offset == 1:
                return  formatString("day_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.day) where offset > 1:
                return  formatString("days_plural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.hour) where offset == 1:
                return  formatString("hour_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.hour) where offset > 1:
                return  formatString("hours_plural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.minute) where offset == 1:
                return  formatString("minute_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.minute) where offset > 1:
                return  formatString("minutes_pural_offset", withOffset: offset)
            case (offset, NSCalendar.Unit.second) where offset == 1:
                return formatString("second_singular_offset", withOffset: nil)
            case (offset, NSCalendar.Unit.second) where offset > 1:
                return formatString("seconds_plural_offset", withOffset: offset)
            default:
                return "unknown time"
            }
        } else {
            return ISCoreConstants.localizedStringForKey("just_now_offset")
        }
    }
}

