//
//  LCViewControllerExtension.swift
//  IlhasoftCore
//
//  Created by Dielson Sales de Carvalho on 21/04/16.
//  Copyright © 2016 Lucy. All rights reserved.
//

import UIKit

public extension UIViewController {
    func displaySubviewController(_ viewController: UIViewController, insideContainer container:UIView) {
        viewController.view.frame.size.width = container.frame.size.width;
        viewController.view.frame.size.height = container.frame.size.height;
        self.addChildViewController(viewController)
        container.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
    }

    func removeSubviewController(_ viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }


    func cycleFromViewController(_ oldVC: UIViewController, toViewController newVC:UIViewController, fromParent parent:UIViewController) {
        // Prepare the two view controllers for the change.
        oldVC.willMove(toParentViewController: nil);
        parent.addChildViewController(newVC);

        // Get the start frame of the new view controller and the end frame
        // for the old view controller. Both rectangles are offscreen.
        newVC.view.frame = oldVC.view.frame;
        let endFrame = oldVC.view.frame;

        // Queue up the transition animation.
        parent.transition(
            from: oldVC,
            to: newVC,
            duration: 0.25,
            options:UIViewAnimationOptions.allowAnimatedContent,
            animations:{
                // Animate the views to their final positions.
                newVC.view.frame = oldVC.view.frame;
                oldVC.view.frame = endFrame;
            },
            completion: {finished in
                // Remove the old view controller and send the final
                // notification to the new view controller.
                oldVC.removeFromParentViewController();
                newVC.didMove(toParentViewController: parent);
            }
        );
    }
}
