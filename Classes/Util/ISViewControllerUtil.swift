//
//  ISViewControllerUtil.swift
//  IlhasoftCore
//
//  Created by Daniel Borges on 30/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

var screenSize: CGSize {
    return UIScreen.main.bounds.size
}

public func switchRootViewController(_ rootViewController: UIViewController, animated: Bool,
                              transition: UIViewAnimationOptions = .transitionFlipFromLeft,
                              completion: (() -> Void)?) {
    let window = UIApplication.shared.keyWindow
    if animated {
        UIView.transition(with: window!, duration: 0.5, options: transition, animations: {
            let oldState: Bool = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            window!.rootViewController = rootViewController
            UIView.setAnimationsEnabled(oldState)
            }, completion: { finished in
                if completion != nil {
                    completion!()
                }
        })
    } else {
        window!.rootViewController = rootViewController
    }
}
