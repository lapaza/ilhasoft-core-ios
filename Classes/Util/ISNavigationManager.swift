//
//  ISNavigationManager.swift
//  Pods
//
//  Created by Daniel Amaral on 17/11/16.
//
//

import UIKit

public enum ISNavigationBarType {
    case Clear
    case Default
}

public class ISNavigationManager {
    
    public class func setupNavigationBarWithType(navigationController:UINavigationController?,setNavigationBarHidden:Bool,type:ISNavigationBarType, tintColor: UIColor = UIColor.black, barStyle: UIBarStyle = UIBarStyle.default, backgroundColor: UIColor = UIColor.blue) {
        
        navigationController?.setNavigationBarHidden(setNavigationBarHidden, animated: true)
        navigationController?.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        navigationController?.navigationItem.backBarButtonItem?.tintColor = tintColor
        navigationController?.navigationBar.barStyle = barStyle
        
        switch type {
        case .Clear:
            UIView.animate(withDuration: 0, animations: { () -> Void in
                navigationController?.navigationBar.isTranslucent = true
                navigationController?.navigationBar.tintColor = tintColor
                navigationController?.navigationBar.shadowImage = UIImage()
                navigationController?.navigationBar.backgroundColor = UIColor.clear
                navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            })
            break
        case .Default:
            UIView.animate(withDuration: 0, animations: { () -> Void in
                navigationController?.navigationBar.isTranslucent = false
                navigationController?.navigationBar.barTintColor = backgroundColor
                navigationController?.navigationBar.tintColor = tintColor
                navigationController?.navigationBar.backgroundColor = backgroundColor
            })
            break
        }
    }
    
}
