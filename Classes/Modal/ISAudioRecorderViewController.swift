//
//  URISAudioRecorderViewController.swift
//  ureport
//
//  Created by Daniel Amaral on 17/02/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import AVFoundation
import Proposer
import IlhasoftCore

public protocol ISAudioRecorderViewControllerDelegate {
    func newAudioRecorded(_ ISAudioRecorderViewController:ISAudioRecorderViewController,media:ISMedia)
}

public class ISAudioRecorderViewController: ISModalViewController, ISAudioRecorderManagerDelegate, AVAudioPlayerDelegate, ISAudioViewDelegate {

    @IBOutlet public weak var lbRecorder: UILabel!
    @IBOutlet public weak var btRecording: UIButton!
    @IBOutlet public weak var btCancel: UIButton!
    @IBOutlet public weak var bgAudioView: UIView!
    
    public var delegate:ISAudioRecorderViewControllerDelegate?
    
    let audioView = Bundle(for: ISAudioRecorderViewController.self).loadNibNamed("ISAudioView", owner: nil, options: nil)?[0] as! ISAudioView
    let maximumTime = 50
    
    public var audioMedia:ISAudioMedia?
    
    public init(audioURL:String?) {
        super.init(nibName: "ISAudioRecorderViewController", bundle: Bundle(for: ISAudioRecorderViewController.self))
        audioView.isRecording = false
        audioView.audioRemoteURL = audioURL
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        self.btRecording.setTitle(ISCoreConstants.localizedStringForKey("Record"), for: .normal)
        self.btCancel.setTitle(ISCoreConstants.localizedStringForKey("Cancel"), for: .normal)
        self.lbRecorder.text = ISCoreConstants.localizedStringForKey("Recorder")
        
        audioView.audioViewdelegate = self
        
        if let audioRemoteURL = audioView.audioRemoteURL {
            audioView.playAudioImmediately(audioRemoteURL,showPreloading: true)
        }else{
            self.btRecording.isHidden = false
            audioView.btPlay.isEnabled = false
            audioView.btPlay.setImage(UIImage(named: "play-icon", in: Bundle(for: self.classForCoder), compatibleWith: nil), for: UIControlState.normal)
            audioView.setupSlider()
            audioView.lbMaxTime.text = audioView.getDurationString(maximumTime)
        }
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        var frame = audioView.frame
        frame.size.width = self.bgAudioView.frame.size.width
        audioView.frame = frame
        
        bgAudioView.addSubview(audioView)
        bgAudioView.layoutSubviews()
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        audioMedia = nil

        if audioView.player != nil && audioView.player.isPlaying == true {
            audioView.player.stop()
        }

    }
    
    //MARK: Button Events
    
    @IBAction func btPlayTapped() {
        audioView.play()
    }
    
    @IBAction func btCancelTapped() {
        self.close()
    }
    
    @IBAction func btRecordTapped() {
        
        if let audioMedia = audioMedia {
            if let delegate = delegate {
                audioMedia.metadata = ["audioDuration":"\(Int(audioView.player.duration))"]
                delegate.newAudioRecorded(self, media: audioMedia)
                self.dismiss(animated: true, completion: nil)
                return
            }
        }
        
        if audioView.isRecording == true {
            audioView.needFinishRecord()
        }else{
            
            proposeToAccess(PrivateResource.microphone, agreed: {
                
                self.audioView.isRecording = true
                self.audioView.audioRecorder = ISAudioRecorderManager()
                self.audioView.audioRecorder.delegate = self
                self.btRecording.setTitle(ISCoreConstants.localizedStringForKey("Stop"), for: UIControlState.normal)
                self.audioView.startTimeRecording = CFAbsoluteTimeGetCurrent()
                self.audioView.audioRecorder.startAudioRecord()
                self.audioView.timerRecording = Timer.scheduledTimer(timeInterval: 1.0, target: self.audioView, selector: #selector(self.audioView.timerCheckOnRecording), userInfo: nil, repeats: true)
                self.audioView.timerRecording.fire()
                
                }, rejected: {
                    self.alertNoPermissionToAccess(PrivateResource.microphone)
            })
            
        }

    }
    
    //MARK: AudioViewDelegate
    
    public func finishRecord() {
        self.btRecording.setTitle(ISCoreConstants.localizedStringForKey("Send"), for: UIControlState.normal)
    }
    
    func didStartPlaying(_ view: ISAudioView) {
        
    }
    
    //MARK: AudioRecorderManagerDelegate
    
    public func audioRecorderDidFinish(_ path: String) {
        
        audioMedia = ISAudioMedia()
        audioMedia!.path = path
        
        audioView.setupAudioPlayerWithURL(ISAudioRecorderManager.outputURLFile)
    }
    
    //MARK: Class Methods
    
}
