//
//  LCLucyAlertViewController.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 17/11/15.
//  Copyright © 2015 Lucy. All rights reserved.
//

import UIKit

public enum LCAlertViewType {
    case cancel
    case yesOrNo
}

public protocol ISImageAlertViewControllerDelegate {
    func yesButtonTapped()
    func noButtonTapped()
    func cancelButtonTapped()
}

open class ISImageAlertViewController: ISModalViewController {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgAlert: UIImageView!
    @IBOutlet weak var optionSeparator: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btNO: UIButton!
    @IBOutlet weak var btYes: UIButton!
    
    var alertViewType:LCAlertViewType!
    var alertTitle:String!
    var message:String!
    var imageName:String!
    var setupBackgroundViewAsRounded:Bool?
    var imgAlertContentMode:UIViewContentMode?
    
    open var delegate:ISImageAlertViewControllerDelegate!
    
    required public init(alertViewType:LCAlertViewType!,alertTitle:String!,message:String!,imageName:String!,setupBackgroundViewAsRounded:Bool!) {
        self.alertTitle = alertTitle
        self.message = message
        self.alertViewType = alertViewType
        self.imageName = imageName
        self.setupBackgroundViewAsRounded = setupBackgroundViewAsRounded
        self.imgAlertContentMode = UIViewContentMode.scaleAspectFit
        super.init(nibName: "ISImageAlertViewController", bundle: Bundle(for: ISImageAlertViewController.self))
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        setupLayout()
    }
    
    //MARK: Class Methods
    
    open func setupImgAlertContentMode(_ contentMode:UIViewContentMode) {
        self.imgAlertContentMode = contentMode
    }
    
    open func setupAlertTitle(_ alertTitle:String) {
        self.alertTitle = alertTitle
        self.lbTitle.text = self.alertTitle
    }
    
    
    open func setupAlertController(_ alertViewType:LCAlertViewType) {
        self.alertViewType = alertViewType
        setupLayout()
    }
    
    open func setupMessage(_ message:String) {
        self.message = message
        self.lbMessage.text = self.message
    }    
    
    func setupLayout() {            
        
        self.lbTitle.text = alertTitle
        self.lbMessage.text = message
        self.viewBackground.layer.cornerRadius = 15
        self.btYes.isHidden = self.alertViewType == .cancel
        self.btNO.isHidden = self.alertViewType == .cancel
        self.btCancel.isHidden = self.alertViewType == .yesOrNo
        self.optionSeparator.isHidden = self.alertViewType == .cancel
        self.imgAlert.image = UIImage(named: self.imageName)
        
        if setupBackgroundViewAsRounded == true {
            self.viewBackground.layer.cornerRadius = 20
        }
        
        self.imgAlert.contentMode = self.imgAlertContentMode!
        
    }
    
    //MARK: Button Events
    
    @IBAction func btNOTapped(_ sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.noButtonTapped()
        }
    }
    
    @IBAction func btYESTapped(_ sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.yesButtonTapped()
        }
    }
    
    @IBAction func btCancelTapped(_ sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.cancelButtonTapped()
        }
    }
}
