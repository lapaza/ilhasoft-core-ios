//
//  URTermsViewController.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 14/03/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

public protocol ISTermsViewControllerDelegate {
    func userDidAcceptTerms(_ accept:Bool)
}

open class ISTermsViewController: ISModalViewController {
    
    @IBOutlet weak var txtTerms: UITextView!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btAccept: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    open var delegate:ISTermsViewControllerDelegate?
    
    fileprivate var fileName:String?
    fileprivate var fileExtension:String?
    fileprivate var setupButtonAsRounded:Bool?
    fileprivate var btAcceptColor:UIColor?
    fileprivate var btCancelColor:UIColor?
    fileprivate var btAcceptTitle:String?
    fileprivate var btCancelTitle:String?
    fileprivate var btAcceptTitleColor:UIColor?
    fileprivate var btCancelTitleColor:UIColor?
    fileprivate var setupBackgroundViewAsRounded:Bool?
    
    required public init(fileName:String,fileExtension:String,btAcceptColor:UIColor?, btCancelColor:UIColor?, btAcceptTitle:String?, btCancelTitle:String?, btAcceptTitleColor:UIColor?, btCancelTitleColor:UIColor?, setupButtonAsRounded:Bool?, setupBackgroundViewAsRounded:Bool?) {
        super.init(nibName: "ISTermsViewController", bundle: Bundle(for: ISTermsViewController.self))
        self.fileName = fileName
        self.fileExtension = fileExtension
        self.btAcceptColor = btAcceptColor
        self.btCancelColor = btCancelColor
        self.btAcceptTitle = btAcceptTitle
        self.btCancelTitle = btCancelTitle
        self.btAcceptTitleColor = btAcceptTitleColor
        self.btCancelTitleColor = btCancelTitleColor
        self.setupButtonAsRounded = setupButtonAsRounded
        self.setupBackgroundViewAsRounded = setupBackgroundViewAsRounded
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    override open func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.txtTerms.scrollRangeToVisible(NSMakeRange(0, 0))
        
        if let fileName = fileName {
            setTerms(fileName, fileExtension: fileExtension!)
        }
    }
    
    //MARK: Class Methods
    
    func setupLayout() {
        self.btAccept.backgroundColor = btAcceptColor
        self.btAccept.titleLabel?.text = btAcceptTitle
        self.btAccept.setTitleColor(btAcceptTitleColor, for: UIControlState())
        
        self.btCancel.backgroundColor = btCancelColor
        self.btCancel.titleLabel?.text = btCancelTitle
        self.btCancel.setTitleColor(btCancelTitleColor, for: UIControlState())
        
        if setupButtonAsRounded == true {
            self.btCancel.layer.cornerRadius = 18
            self.btAccept.layer.cornerRadius = 18
        }
        
        if setupBackgroundViewAsRounded == true {
            self.viewBackground.layer.cornerRadius = 20
        }
        
        setTerms(fileName!, fileExtension: fileExtension!)
    }
    
    open func setTerms(_ fileName:String,fileExtension:String) {
        
        if let rtfPath = Bundle.main.url(forResource: fileName, withExtension: fileExtension) {
            do {
                let attributedStringWithRtf = try NSAttributedString(fileURL: rtfPath, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil)
                
                self.txtTerms.attributedText = attributedStringWithRtf
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
    }
    
    //MARK: Button Events

    @IBAction func btCancelTapped(_ sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.userDidAcceptTerms(false)
        }
    }
    
    @IBAction func btAcceptTapped(_ sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.userDidAcceptTerms(true)
        }
    }
}
