//
//  ISAudioMedia.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public class ISAudioMedia: ISMedia {
    public var path:String!
    public var metadata:NSDictionary!
}
