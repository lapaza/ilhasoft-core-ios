//
//  ISMedia.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public enum ISMediaType: String {
    case audio   = "audio"
    case picture = "picture"
    case video   = "video"
    case file    = "file"
    case youtube = "youtube"
}

public class ISMedia: NSObject {

    public var id:String!
    public var url:String!
    public var type: ISMediaType {
        if self is ISAudioMedia {
            return .audio
        }else if self is ISImageMedia {
            return .picture
        }else if self is ISVideoMedia {
            return .video
        }else if self is ISFileMedia {
            return .file
        }else {
            return .youtube
        }
    }
    public var thumbnail:String!
    
    public init(id:String,url:String,thumbnail:String) {
        self.id = id
        self.url = url
        self.thumbnail = thumbnail
        super.init()
    }
    
    override public init() {
        super.init()
    }
    
}
