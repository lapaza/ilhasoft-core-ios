//
//  ISVideoMidia.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public class ISYoutubeMedia: ISMedia {
    public var path:String!
    public var thumbnailImage:UIImage!
}
